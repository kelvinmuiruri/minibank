package com.minibank.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserAccount.class)
public abstract class UserAccount_ {

	public static volatile SingularAttribute<UserAccount, String> firstName;
	public static volatile SingularAttribute<UserAccount, String> lastName;
	public static volatile SingularAttribute<UserAccount, String> emailAddress;
	public static volatile SingularAttribute<UserAccount, Date> lastUpdatedDate;
	public static volatile SingularAttribute<UserAccount, String> sirName;
	public static volatile SingularAttribute<UserAccount, Date> createdDate;
	public static volatile SingularAttribute<UserAccount, String> accountType;
	public static volatile SingularAttribute<UserAccount, Double> currentBalance;
	public static volatile SingularAttribute<UserAccount, Long> userId;
	public static volatile SingularAttribute<UserAccount, String> idNo;
	public static volatile SingularAttribute<UserAccount, Date> birthDate;

}

