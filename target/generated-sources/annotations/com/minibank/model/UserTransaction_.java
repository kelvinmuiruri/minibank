package com.minibank.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserTransaction.class)
public abstract class UserTransaction_ {

	public static volatile SingularAttribute<UserTransaction, String> transactionType;
	public static volatile SingularAttribute<UserTransaction, String> note;
	public static volatile SingularAttribute<UserTransaction, Double> amount;
	public static volatile SingularAttribute<UserTransaction, Double> initialBalance;
	public static volatile SingularAttribute<UserTransaction, Double> closingBalance;
	public static volatile SingularAttribute<UserTransaction, Date> transactionDate;
	public static volatile SingularAttribute<UserTransaction, Long> userId;
	public static volatile SingularAttribute<UserTransaction, Long> transactionId;
	public static volatile SingularAttribute<UserTransaction, Integer> frequency;

}

