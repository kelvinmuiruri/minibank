package com.minibank.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Limitation.class)
public abstract class Limitation_ {

	public static volatile SingularAttribute<Limitation, String> transactionType;
	public static volatile SingularAttribute<Limitation, Double> maxPerTransction;
	public static volatile SingularAttribute<Limitation, Long> limitId;
	public static volatile SingularAttribute<Limitation, Double> maxPerDay;
	public static volatile SingularAttribute<Limitation, Integer> frequencyPerDay;

}

