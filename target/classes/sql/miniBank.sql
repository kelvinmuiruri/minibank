/*
Navicat MySQL Data Transfer

Source Server         : kevol
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : miniBank

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-08-18 22:19:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `limitation`
-- ----------------------------
DROP TABLE IF EXISTS `limitation`;
CREATE TABLE `limitation` (
  `limit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `frequency_per_day` int(11) DEFAULT NULL,
  `max_per_day` double NOT NULL,
  `max_per_transction` double NOT NULL,
  `transaction_type` longtext NOT NULL,
  PRIMARY KEY (`limit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of limitation
-- ----------------------------
INSERT INTO `limitation` VALUES ('1', '4', '150000', '50000', 'deposit');
INSERT INTO `limitation` VALUES ('2', '3', '40000', '20000', 'withdrawal');

-- ----------------------------
-- Table structure for `user_account`
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(200) DEFAULT NULL,
  `birth_date` datetime NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `current_balance` double DEFAULT NULL,
  `email_address` longtext NOT NULL,
  `first_name` longtext NOT NULL,
  `id_no` varchar(20) NOT NULL,
  `last_name` longtext NOT NULL,
  `last_updated_date` datetime NOT NULL,
  `sir_name` longtext,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
  
-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES ('1', 'personal', '2017-08-07 22:26:56', '2017-08-01 22:27:04', '15500', 'kevol@gmail.com', 'kelvin', '35658946', 'muiruri', '2017-08-18 15:16:58', 'ffffffff');

-- ----------------------------
-- Table structure for `user_transaction`
-- ----------------------------
DROP TABLE IF EXISTS `user_transaction`;
CREATE TABLE `user_transaction` (
  `transaction_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `closing_balance` double DEFAULT NULL,
  `frequency` int(11) NOT NULL,
  `initial_balance` double DEFAULT NULL,
  `note` longtext,
  `transaction_date` datetime NOT NULL,
  `transaction_type` longtext NOT NULL,
  `account_id` bigint(20) NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `FK9uujtrj0u32w32bjxp9i2eciv` (`account_id`),
  CONSTRAINT `FK9uujtrj0u32w32bjxp9i2eciv` FOREIGN KEY (`account_id`) REFERENCES `user_account` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_transaction
-- ----------------------------
INSERT INTO `user_transaction` VALUES ('1', '500', '600', '1', '100', 'frffgdeposit', '2017-08-17 22:28:47', 'deposit', '1');
INSERT INTO `user_transaction` VALUES ('2', '5000', '10600', '1', '5600', 'kkcdcjdbhdbvdhbvdbvbdvbdbvdb', '2017-08-18 14:36:59', 'deposit', '1');
INSERT INTO `user_transaction` VALUES ('3', '5000', '15600', '1', '10600', 'kkcdcjdbhdbvdhbvdbvbdvbdbvdb', '2017-08-18 14:45:30', 'deposit', '1');
INSERT INTO `user_transaction` VALUES ('4', '100', '15500', '1', '15600', 'withdrawal', '2017-08-18 15:16:58', 'withdrawal', '1');
