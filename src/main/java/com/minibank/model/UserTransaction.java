/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kevol
 */
@Entity
@Table(name = "user_transaction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserTransaction.findAll", query = "FROM UserTransaction u WHERE u.userId= '"+1+"'  ORDER BY u.transactionId DESC"),
    @NamedQuery(name = "UserTransaction.findByTransactionId", query = "SELECT u FROM UserTransaction u WHERE u.transactionId = :transactionId"),
    @NamedQuery(name = "UserTransaction.findByTransactionType", query = "SELECT u FROM UserTransaction u WHERE u.transactionType = :transactionType"),
    @NamedQuery(name = "UserTransaction.findByAmount", query = "SELECT u FROM UserTransaction u WHERE u.amount = :amount"),
    @NamedQuery(name = "UserTransaction.findByInitialBalance", query = "SELECT u FROM UserTransaction u WHERE u.initialBalance = :initialBalance"),
    @NamedQuery(name = "UserTransaction.findByClosingBalance", query = "SELECT u FROM UserTransaction u WHERE u.closingBalance = :closingBalance"),
    @NamedQuery(name = "UserTransaction.findByNote", query = "SELECT u FROM UserTransaction u WHERE u.note = :note"),
    @NamedQuery(name = "UserTransaction.findByTransactionDate", query = "SELECT u FROM UserTransaction u WHERE u.transactionDate = :transactionDate"),
        @NamedQuery(name = "UserTransaction.accountnumber", query = "SELECT u FROM UserTransaction u WHERE u.userId = :userId")})
public class UserTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
     public static final String FIND_ALL = "UserTransaction.findAll"; 
      public static final String FIND_BYACCOUNT = "UserTransaction.accountnumber"; 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "transaction_id")
    private Long transactionId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "transaction_type")
    private String transactionType;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Column(name = "initial_balance")
    private Double initialBalance;
    @Column(name = "closing_balance")
    private Double closingBalance;
    @Size(max = 500)
    @Column(name = "note")
    private String note;
    @Basic(optional = false)
    @NotNull
    @Column(name = "transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "frequency")
    private int frequency;
     @NotNull
    @Column(name = "account_id")
    private Long userId;

    public UserTransaction() {
    }

    public UserTransaction(Long transactionId) {
        this.transactionId = transactionId;
    }

    public UserTransaction(Long transactionId, String transactionType, Date transactionDate, int frequency) {
        this.transactionId = transactionId;
        this.transactionType = transactionType;
        this.transactionDate = transactionDate;
        this.frequency = frequency;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(Double initialBalance) {
        this.initialBalance = initialBalance;
    }

    public Double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(Double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionId != null ? transactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTransaction)) {
            return false;
        }
        UserTransaction other = (UserTransaction) object;
        if ((this.transactionId == null && other.transactionId != null) || (this.transactionId != null && !this.transactionId.equals(other.transactionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.minibank.model.UserTransaction[ transactionId=" + transactionId + " ]";
    }
    
}
