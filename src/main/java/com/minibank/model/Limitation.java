/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kevol
 */
@Entity
@Table(name = "limitation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Limitation.findAll", query = "SELECT l FROM Limitation l"),
    @NamedQuery(name = "Limitation.findByLimitId", query = "SELECT l FROM Limitation l WHERE l.limitId = :limitId"),
    @NamedQuery(name = "Limitation.findByTransactionType", query = "SELECT l FROM Limitation l WHERE l.transactionType = :transactionType")})
public class Limitation implements Serializable {

    private static final long serialVersionUID = 1L;
        public static final String Find_ByTransactionType = "Limitation.findByTransactionType"; 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "limit_id")
    private Long limitId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "transaction_type")
    private String transactionType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "max_per_day")
    private double maxPerDay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "max_per_transction")
    private double maxPerTransction;
    @Column(name = "frequency_per_day")
    private Integer frequencyPerDay;

    public Limitation() {
    }

    public Limitation(Long limitId) {
        this.limitId = limitId;
    }

    public Limitation(Long limitId, String transactionType, double maxPerDay, double maxPerTransction) {
        this.limitId = limitId;
        this.transactionType = transactionType;
        this.maxPerDay = maxPerDay;
        this.maxPerTransction = maxPerTransction;
    }

    public Long getLimitId() {
        return limitId;
    }

    public void setLimitId(Long limitId) {
        this.limitId = limitId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public double getMaxPerDay() {
        return maxPerDay;
    }

    public void setMaxPerDay(double maxPerDay) {
        this.maxPerDay = maxPerDay;
    }

    public double getMaxPerTransction() {
        return maxPerTransction;
    }

    public void setMaxPerTransction(double maxPerTransction) {
        this.maxPerTransction = maxPerTransction;
    }

    public Integer getFrequencyPerDay() {
        return frequencyPerDay;
    }

    public void setFrequencyPerDay(Integer frequencyPerDay) {
        this.frequencyPerDay = frequencyPerDay;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (limitId != null ? limitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Limitation)) {
            return false;
        }
        Limitation other = (Limitation) object;
        if ((this.limitId == null && other.limitId != null) || (this.limitId != null && !this.limitId.equals(other.limitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.minibank.model.Limitation[ limitId=" + limitId + " ]";
    }
    
}
