/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.services;

import com.minibank.interfaces.commonoperation;
import com.minibank.model.Limitation;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 *
 * @author kevol
 */
@Stateless
@Path("limit")
public class Transactioncontrol extends commonoperation{
            @PersistenceContext(unitName = "com.minibank_minibank_war_1.0-SNAPSHOTPU")
            private EntityManager em;

      
   @POST
    @Path("rule")
    @Consumes(APPLICATION_FORM_URLENCODED)
         @Produces({ MediaType.APPLICATION_JSON})
    public Response authenticateUser(@FormParam("transactype") String transactiontype) {
        List<Limitation> limitcaps = new ArrayList<>();
        try {
           limitcaps = this.findLimit(transactiontype);
        } catch (Exception e) {
            return Response.status(UNAUTHORIZED).build();
        }
        return Response.ok(limitcaps).build();
    }
     @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Limitation entity) {
      em.merge(entity);
    }
    
    
    
    
}
