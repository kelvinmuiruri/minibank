/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.services;

import com.minibank.model.UserAccount;
import com.minibank.model.UserTransaction;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 *
 * @author kevol
 */
@Stateless
@Path("find")
public class AccountReport {
     @PersistenceContext(unitName = "com.minibank_minibank_war_1.0-SNAPSHOTPU")
    private EntityManager em;
     
     
     
        @GET
        @Path("balance")
        @Produces({ MediaType.APPLICATION_JSON})
    public Response findAllUsers() {
              List<UserAccount> Usersbalance = new ArrayList<>();
              if(Usersbalance!=null && !Usersbalance.isEmpty()){Usersbalance.clear();}
          TypedQuery<UserAccount> query = em.createNamedQuery(UserAccount.FindByUserId, UserAccount.class)
                     .setParameter("userId", 1l)
                     ;
             Usersbalance = query.getResultList();

           if (Usersbalance == null){
            return Response.status(NOT_FOUND).build();
           }
        return Response.ok(Usersbalance).build();
    }
    
           @GET
        @Path("transactions")
        @Produces({ MediaType.APPLICATION_JSON})
    public Response findAlltransaction() {
              List<UserTransaction> usertransaction = new ArrayList<>();
              if(usertransaction!=null && !usertransaction.isEmpty()){usertransaction.clear();}
          TypedQuery<UserTransaction> query = em.createNamedQuery(UserTransaction.FIND_ALL, UserTransaction.class) ; 
          usertransaction = query.getResultList();

           if (usertransaction == null){
            return Response.status(NOT_FOUND).build();
           }
        return Response.ok(usertransaction).build();
    }
}
