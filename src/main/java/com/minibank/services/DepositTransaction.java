/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.services;

import com.minibank.interfaces.BankFunction;
import com.minibank.model.Limitation;
import com.minibank.model.UserAccount;
import com.minibank.model.UserTransaction;
import static com.minibank.services.Withdrawals.transactionType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 *
 * @author kevol
 */
@Stateless
@Path("debit")
public class DepositTransaction extends BankFunction {

    static final String transactionType = "deposit";
    @PersistenceContext(unitName = "com.minibank_minibank_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @POST
    @Path("deposit")
    @Consumes(APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_JSON})
    public Response debit(@FormParam("amount") double depositAmount,
            @FormParam("accountid") long accountId,
            @FormParam("note") String note) {
        double dailytotalAmount = 0.0;
        long daily_frequency = 0;

        Limitation limitation = new Limitation();
        List<UserAccount> allUsers = new ArrayList<>();
        try {
            dailytotalAmount = this.findtotaldailytransaction(transactionType);
            daily_frequency = this.finddailyfrequency(transactionType);
            limitation = this.findTransactionLimit(transactionType);
            double previousbal = this.findBalance(accountId).getCurrentBalance();
            System.out.println(" dailyAmount " + dailytotalAmount + "dailyfreq " + daily_frequency + "accoprevious balunt id" + previousbal);
            if (limitation != null) {
                if (dailytotalAmount <= limitation.getMaxPerDay() && daily_frequency <= limitation.getFrequencyPerDay()
                        && depositAmount <= limitation.getMaxPerTransction()) {
                    try {
                        UserTransaction userTransaction = new UserTransaction();

                        userTransaction.setAmount(depositAmount);
                        userTransaction.setTransactionType(transactionType);
                        userTransaction.setInitialBalance(previousbal);
                        userTransaction.setClosingBalance(previousbal + depositAmount);
                        userTransaction.setNote(note);
                        userTransaction.setFrequency(1);
                        userTransaction.setTransactionDate(new Date());
                        userTransaction.setUserId(accountId);
                        em.persist(userTransaction);

                        System.out.println(" Amount " + depositAmount + " " + "account id" + accountId);
                        UserAccount userAccount = em.find(UserAccount.class, accountId);
                        userAccount.setCurrentBalance(previousbal + depositAmount);
                        userAccount.setLastUpdatedDate(new Date());
                        em.merge(userAccount);
                    } catch (Exception l) {
                        return Response.status(UNAUTHORIZED).build();
                    }
                } else {
                    return Response.status(UNAUTHORIZED).build();
                }
            } else {
                return Response.status(UNAUTHORIZED).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(UNAUTHORIZED).build();
        }
        return Response.ok(allUsers).build();
    }
}
