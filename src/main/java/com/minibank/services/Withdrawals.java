/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.services;

import com.minibank.interfaces.BankFunction;
import com.minibank.model.Limitation;
import com.minibank.model.UserAccount;
import com.minibank.model.UserTransaction;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 *
 * @author kevol
 */
@Stateless
@Path("credit")
public class Withdrawals extends BankFunction {

    static final String transactionType = "withdrawal";
    @PersistenceContext(unitName = "com.minibank_minibank_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @POST
    @Path("withdrawal")
    @Consumes(APPLICATION_FORM_URLENCODED)
          @Produces({ MediaType.APPLICATION_JSON})
    public Response credit(@FormParam("amount") double withdrawalAmount,
            @FormParam("accountId") long accountId,
            @FormParam("note") String note) {
        double dailytotalAmount = 0.0;
        long daily_frequency = 0;
        Limitation limitation = new Limitation();
        UserAccount userAccount = new UserAccount();
        List<UserAccount> allUsers = null;
        try {
            dailytotalAmount = this.findtotaldailytransaction(transactionType);
            daily_frequency = this.finddailyfrequency(transactionType);
            userAccount = this.findBalance(accountId);
              double previousbal = this.findBalance(accountId).getCurrentBalance();
            limitation = this.findTransactionLimit(transactionType);
            if (userAccount.getCurrentBalance() >= withdrawalAmount && limitation != null) {
                if (dailytotalAmount <= limitation.getMaxPerDay() && daily_frequency <= limitation.getFrequencyPerDay()
                        && withdrawalAmount <= limitation.getMaxPerTransction()) {
                    System.out.println(" initiating crediting transaction of  " + withdrawalAmount + " by " + "account id" + accountId);

                    
                    try{
                    UserTransaction userTransaction = new UserTransaction();
                    userTransaction.setAmount(withdrawalAmount);
                    userTransaction.setTransactionType(transactionType);
                    userTransaction.setInitialBalance(previousbal);
                    userTransaction.setClosingBalance(previousbal - withdrawalAmount);
                    userTransaction.setNote(note);
                    userTransaction.setFrequency(1);
                    userTransaction.setTransactionDate(new Date());
                    userTransaction.setUserId(accountId);
                    em.persist(userTransaction);

                    System.out.println(" Amount withdrawn  " + withdrawalAmount + " " + "account id" + accountId);
                    UserAccount userAccount2 = em.find(UserAccount.class, accountId);
                    userAccount2.setCurrentBalance(previousbal - withdrawalAmount);
                    userAccount2.setLastUpdatedDate(new Date());
                    em.merge(userAccount2);
}catch(Exception l){
 return Response.status(UNAUTHORIZED).build();
}
                    
                    
                    
                    
                    
                    
                    
                } else {
                   return Response.status(UNAUTHORIZED).build();
                }
            } else {
               return Response.status(UNAUTHORIZED).build();
            }
        } catch (Exception e) {
            return Response.status(UNAUTHORIZED).build();
        }
        return Response.ok(allUsers).build();
    }
}
