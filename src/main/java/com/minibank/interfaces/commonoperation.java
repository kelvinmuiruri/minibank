/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.interfaces;

import com.minibank.model.Limitation;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author mspace
 */
public abstract class commonoperation {
     @PersistenceContext(unitName = "com.minibank_minibank_war_1.0-SNAPSHOTPU")
    private EntityManager em;
public List<Limitation> findLimit(String transactiontype) {
        List <Limitation> findlimits = new ArrayList<>();
        try{
        TypedQuery<Limitation> query = em.createNamedQuery(Limitation.Find_ByTransactionType, Limitation.class)
                .setParameter("transactionType", transactiontype);
        findlimits = query.getResultList();
        }catch(Exception k){
              System.out.println("Getting Account balance  error ");
                }
        return findlimits;
    }
}
