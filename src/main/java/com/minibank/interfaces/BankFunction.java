/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.interfaces;

import com.minibank.model.Limitation;
import com.minibank.model.UserAccount;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author kevol
 */
public abstract class BankFunction {

    @PersistenceContext(unitName = "com.minibank_minibank_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public long getdaydifference(String previousDate) {
        long chronoUnits = 0l;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime firstLocalDateTime = LocalDateTime.parse(previousDate, formatter);
            LocalDateTime secondLocalDateTime = LocalDateTime.now();
            chronoUnits = ChronoUnit.DAYS.between(firstLocalDateTime, secondLocalDateTime);
        } catch (Exception k) {
            System.out.println("Date coonversion Error");
        }
        return chronoUnits;
    }

    public double findtotaldailytransaction(String transactiontype) {
        double daily_amount = 0.0;
        Number result = 0.0;
        try {
            Query q = em.createQuery("SELECT SUM(m.amount) FROM UserTransaction m where  m.transactionType='"+transactiontype+"' and date(m.transactionDate) = current_date");
            result = (Number) q.getSingleResult();
            
        daily_amount = result.doubleValue();
        System.out.println("Total Amount" + daily_amount);
        } catch (Exception k) {
            System.out.println("summation return 0");
            daily_amount = 0.0;
        }
    
        return daily_amount;
    }

    public long finddailyfrequency(String transactiontype) {
        long frequency = 0;
        try {
            Query query = em.createQuery("SELECT COUNT(c) FROM UserTransaction c where   c.transactionType='"+transactiontype+"' and date(c.transactionDate) = current_date ");
            frequency = (long) query.getSingleResult();
        } catch (Exception k) {
            frequency = 0;
            System.out.println("summation return 0");
        }
        System.out.println("Frequency " + frequency);
        return frequency;
    }

    public Limitation findTransactionLimit(String transactiontype) {
        Limitation limitation = new Limitation();
        try{
        TypedQuery<Limitation> query = em.createNamedQuery(Limitation.Find_ByTransactionType, Limitation.class)
                .setParameter("transactionType", transactiontype);
        limitation = query.getResultList().get(0);
        }catch(Exception k){
            k.printStackTrace();
              System.out.println("Getting Account transaction limitation error ");
                }
        return limitation;
    }
       public UserAccount findBalance(long accountid) {
        UserAccount userAccount = new UserAccount();
        try{
        TypedQuery<UserAccount> query = em.createNamedQuery(UserAccount.FindByUserId, UserAccount.class)
                .setParameter("userId", accountid);
        userAccount = query.getResultList().get(0);
        }catch(Exception k){
            k.printStackTrace();
              System.out.println("Getting Account balance  error ");
                }
        return userAccount;
    }
       
  
       

       
       
}
