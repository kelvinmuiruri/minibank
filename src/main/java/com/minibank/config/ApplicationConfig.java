/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minibank.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author kevol
 */  
@javax.ws.rs.ApplicationPath("account/api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;  
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.minibank.services.AccountReport.class);
        resources.add(com.minibank.services.DepositTransaction.class);
        resources.add(com.minibank.services.Transactioncontrol.class);
        resources.add(com.minibank.services.Withdrawals.class);
        resources.add(com.minibank.utill.NewCrossOriginResourceSharingFilter.class);
    }
   
}
